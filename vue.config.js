// vue.config.js
var webpack = require('webpack');
const path = require('path')

module.exports = {
  chainWebpack: config => {
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => {
        options.limit = -1
        return options
      })
  },
  configureWebpack: {
    module: {
      rules: [

      ],
    },
    resolve: {
      alias: {
        '#assets': path.resolve(__dirname, 'src/assets'),
        '@': path.resolve(__dirname, 'src'),
        '@assets': path.resolve(__dirname, 'src/assets')
      }
    }
  }
}