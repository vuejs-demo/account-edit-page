import Vue from 'vue'
import lodash from 'lodash'
import VeeValidate from 'vee-validate'

Vue.config.productionTip = false

class Setup {
  constructor() {
    this.initLoad()
    this.configGlobal()
  }

  initLoad = () => {
    window._ = lodash
    Vue.use(VeeValidate)
  }

  configGlobal = () => {

  }
}

new Setup()
